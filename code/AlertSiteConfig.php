<?php

class AlertSiteConfig extends DataExtension {

    private static $db = array(
        'HomePageAlert' => 'HTMLText',
        'AlertInternalExternal'=>'Varchar(255)',
        'AlertInternalLink'=>'Varchar(255)',
        'AlertExternalLink'=>'Varchar(255)'
    );

    public function updateCMSFields(FieldList $fields) {
        HtmlEditorConfig::set_active('textonly');
        $fields->addFieldsToTab("Root.Main", HeaderField::create('AlertHeader1', 'Alert for above Banner', 3));
        $fields->addFieldsToTab('Root.Main', LiteralField::create('AlertLiteral1', '<p class="message error" style="margin-top:20px;">Set the Alert text that will appear above the Feature image if set.</p>'));
        $fields->addFieldsToTab('Root.Main', HtmlEditorField::create('HomePageAlert', 'Home Page Alert Text'));
        $fields->addFieldsToTab("Root.Main", OptionsetField::create('AlertInternalExternal', 'Internal or External Link', array('Internal' => 'Internal (link to a page on your website)', 'External' => 'External (link to an external website)')));
        $fields->addFieldsToTab("Root.Main", TreeDropdownField::create('AlertInternalLink', 'Internal Link', 'SiteTree')->setDescription('Please enter the full URL'));
        $fields->addFieldsToTab("Root.Main", TextField::create('AlertExternalLink', 'External Link (include full URL http://domain.com)')->setDescription('Please enter the full URL'));
    }

}