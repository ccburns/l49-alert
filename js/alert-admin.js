jQuery(document).ready(function($) {

    if($("#Form_EditForm_InternalExternal_Internal:checked").length > 0){
        $('#Form_EditForm_InternalLink_Holder').show();
    }else{
        $('#Form_EditForm_InternalLink_Holder').hide();
    }
    if($("#Form_EditForm_InternalExternal_External:checked").length > 0){
        $('#Form_EditForm_ExternalLink_Holder').show();
    }else{
        $('#Form_EditForm_ExternalLink_Holder').hide();
    }

    $(document).on("change", "#Form_EditForm_InternalExternal_Internal", function(e){
        e.preventDefault();
        $('#Form_EditForm_InternalLink_Holder').hide();
        $('#Form_EditForm_ExternalLink_Holder').hide();

        $('#Form_EditForm_InternalLink_Holder').show();
        $('#Form_EditForm_InternalLink_Holder').val(true);
    })

    $(document).on("change", "#Form_EditForm_InternalExternal_External", function(e){
        e.preventDefault();
        $('#Form_EditForm_InternalLink_Holder').hide();
        $('#Form_EditForm_ExternalLink_Holder').hide();

        $('#Form_EditForm_ExternalLink_Holder').show();
        $('#Form_EditForm_ExternalLink_Holder').val(true);
    })

    if($("#Form_EditForm_AlertInternalExternal_Internal:checked").length > 0){
        $('#Form_EditForm_AlertInternalLink_Holder').show();
    }else{
        $('#Form_EditForm_AlertInternalLink_Holder').hide();
    }
    if($("#Form_EditForm_AlertInternalExternal_External:checked").length > 0){
        $('#Form_EditForm_AlertExternalLink_Holder').show();
    }else{
        $('#Form_EditForm_AlertExternalLink_Holder').hide();
    }

    $(document).on("change", "#Form_EditForm_AlertInternalExternal_Internal", function(e){
        e.preventDefault();
        $('#Form_EditForm_AlertInternalLink_Holder').hide();
        $('#Form_EditForm_AlertExternalLink_Holder').hide();

        $('#Form_EditForm_AlertInternalLink_Holder').show();
        $('#Form_EditForm_AlertExternalLink_Holder').val(true);
    })

    $(document).on("change", "#Form_EditForm_AlertInternalExternal_External", function(e){
        e.preventDefault();
        $('#Form_EditForm_AlertInternalLink_Holder').hide();
        $('#Form_EditForm_AlertExternalLink_Holder').hide();

        $('#Form_EditForm_AlertExternalLink_Holder').show();
        $('#Form_EditForm_AlertExternalLink_Holder').val(true);
    })


});